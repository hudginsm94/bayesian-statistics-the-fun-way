# 1
cat('EX 1: One roll of two 6d.\n')
count <- 0
for(roll1 in c(1:6)){
    for(roll2 in c(1:6)){
        count <- count + ifelse(roll1+roll2 > 7,1,0)
    }
}
cat('1st method: \n','P(number > 7)=',count,'/',6**2,'\n')
x <- sum(apply(expand.grid(c(1:6),c(1:6)),1,sum)>7)
cat('2nd method: \n','P(number > 7)=',x,'/',6**2,'\n\n')

# 2
cat('EX 2: One roll of three 6d.\n')
count <- 0
for(roll1 in c(1:6)){
    for(roll2 in c(1:6)){
        for(roll3 in c(1:6)){
            count <- count + ifelse(roll1+roll2+roll3 > 7,1,0)
        }
    }
}
cat('1st method: \n','P(number > 7)=',count,'/',6**3,'\n')
x <- sum(apply(expand.grid(c(1:6),c(1:6),c(1:6)),1,sum)>7)
cat('2nd method: \n','P(number > 7)=',x,'/',6**3,'\n\n')

# 3
cat('Ex 3: $30 to $5 that Red Soxs win. \n')
odds <- 30/5
cat('P(Red Soxs wins)=',odds,'/',odds+1)
